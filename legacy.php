<?php

require_once('vendor/autoload.php');

use BitWasp\Bitcoin\Address\PayToPubKeyHashAddress;
use BitWasp\Bitcoin\Bitcoin;
use BitWasp\Bitcoin\Key\Factory\HierarchicalKeyFactory;
use BitWasp\Bitcoin\Network\NetworkFactory;

class legacy{
	
	private $network = null;
	private $xpub = null;
	private $path = null;
	
	public function __construct($network,$xpub,$path){
		$this->network = $network;
		$this->xpub = $xpub;
		$this->path = $path;
	
	}
	
	public function address(){
		$set = $this->network;
		$net = NetworkFactory::$set();
        $network = Bitcoin::setNetwork($net);
        $hdFactory = new HierarchicalKeyFactory();
        $master = $hdFactory->fromExtended($this->xpub);
        $key1 = $master->deriveChild($this->path);
        $child1 = new PayToPubKeyHashAddress($key1->getPublicKey()->getPubKeyHash());
        $new_address = $child1->getAddress();
        return $new_address;
	}
}

$ali = new legacy('dogecoinTestnet','tpubD9Lb7nPUswKpannTXfNMiUB3FXsCzaKNc89r8rUUpafXBxeeaDh6bLStbu7P9JDC4PZ8T2aKM4Lwdqe735civRANjYJK22y19EgKM96tww6',0);

echo $ali->address();